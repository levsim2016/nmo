from django.db import models
from django.conf import settings
from django.db.models.signals import pre_delete, post_delete
from django.dispatch import receiver

import os, shutil
from zipfile import ZipFile

class Gallery(models.Model):
    title = models.CharField(verbose_name="Название галереи изображений", max_length=256,blank=True, default="Не задано")
    archive = models.FileField(blank=True, verbose_name='Архив изображений (в формате ZIP)', upload_to='')

    def save(self, *args, **kwargs):
        with ZipFile(self.archive, 'r') as zip:
            folderPath = os.path.join(settings.MEDIA_ROOT, str(self.title))
            if os.path.exists(folderPath):
                shutil.rmtree(folderPath)
            galleries = Gallery.objects.filter(title=self.title)
            for gallery in galleries:
                gallery.delete()
            imagesList = GalleryImage.objects.filter(gallery=self)
            if len(imagesList) != 0:
                for image in imagesList:
                    image.delete()

            super().save(*args, **kwargs)
            for name in zip.namelist():
                #print(name.encode('cp437').decode('cp866'))
                preparedName = name.encode('cp437').decode('cp866')
                joinedPath = os.path.join(folderPath, preparedName)
                zip.extract(name, folderPath)
                os.rename(os.path.join(folderPath, name), os.path.join(folderPath, preparedName))
                image = GalleryImage()
                image.imageName = os.path.join(self.title, preparedName)
                image.gallery = self
                image.save()
        archivePath = os.path.join(settings.MEDIA_ROOT, self.archive.name)
        os.remove(archivePath)
            #zip.extractall(os.path.join(settings.MEDIA_ROOT, str(self.date)))
				
    @receiver(pre_delete)
    def delete_images(sender, instance, using, **kwargs):
        if(sender == Gallery):          
            path = os.path.join(settings.MEDIA_ROOT, str(instance.title))
            try:
                shutil.rmtree(path)
            except:
                print("Images don't exist!")

    def get_list_of_image_urls(self):
        urls = []
        for image in GalleryImage.objects.filter(gallery=self):
            urls.append(image.imageName)
        return urls

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = 'Галерея изображений'
        verbose_name_plural = 'Галереи изображений'

class GalleryImage(models.Model):
		gallery = models.ForeignKey(Gallery, related_name='images', on_delete=models.CASCADE, unique=False)
		imageName = models.CharField(verbose_name='Изображение', max_length=512)

		def __str__(self):
				path = os.path.split(self.imageName)
				datePath = os.path.split(path[0])[1]
				return os.path.join(datePath, path[1])

		class Meta:
				verbose_name = 'Фото'
				verbose_name_plural = 'Фото'