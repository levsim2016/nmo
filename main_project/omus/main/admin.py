from django.contrib import admin
from .models import News, Event, Gallery

class NewsAdmin(admin.ModelAdmin):
    fields = ('publicationDate', 'previewImage', 'title', 'link', 'category', 'content', 'gallery')
    readonly_fields = ['link']
    list_display = ['title', 'link', 'publicationDate']
    list_filter = ['publicationDate', 'title']
    search_fields = ['publicationDate', 'title']
    
    

class EventAdmin(admin.ModelAdmin):
    fields = ('date', 'title', 'link', 'description')
    readonly_fields = ['link']
    list_display = ['title', 'link', 'date']
    list_filter = ['date']
    search_fields = ['date', 'title']

class GalleryAdmin(admin.ModelAdmin):
    fields = ('title', 'archive')
    list_display = ['title']
    search_fields = ['title']

admin.site.register(News, NewsAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Gallery, GalleryAdmin)
# Register your models here.
