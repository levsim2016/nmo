from django.db import models
from .event import Event
from .news import News
from .gallery import Gallery, GalleryImage