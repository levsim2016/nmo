from django.shortcuts import render, reverse
from django.shortcuts import HttpResponse, HttpResponseRedirect
from django.http import Http404, JsonResponse

from .models import News, Event

from datetime import datetime
import locale

def redirect_to_news_page(request):
    return HttpResponseRedirect(reverse("news"))
    #return HttpResponseRedirect("news/")

#в этом представлении возвращает только обычные новости
def get_main_page(request, slug=None, date=None, isNews=True):
    try:
        locale.setlocale(locale.LC_ALL, 'ru')
        cutValue = None
    except:
        locale.setlocale(locale.LC_ALL, 'ru_RU')
        cutValue = -1
    #возвращаем все новости и события
    month = int(request.COOKIES.get("calendarMonth"))
    if month < 1 or month > 12:
        monthEvents = Event.objects.filter(date__month=datetime.now().month)
    else:
        monthEvents = Event.objects.filter(date__month=month)
    preparedMonthEvents = []
    for i in range(0, len(monthEvents)):
        if len(monthEvents[i].title) > 20:
            preparedTitle = monthEvents[i].title[0:20] + "..."
        else:
            preparedTitle = monthEvents[i].title
        preparedMonthEvents.append({ "numberOfDay": monthEvents[i].date.day,
                            "wordOfDay": monthEvents[i].date.strftime("%a")[0:cutValue],
                            "title": preparedTitle,
                            "slug": monthEvents[i].slug,
                            "date": monthEvents[i].date })

    if isNews:
        if slug != None:
            currentNews = News.objects.filter(slug=slug, category=News.NEWS)
            if len(currentNews) == 0:
                raise Http404()
            else:
                print("По данной ссылке находится новость!")
                gallery = None
                if currentNews[0].gallery != None:
                    gallery = currentNews[0].gallery.get_list_of_image_urls()
                return render(request, "news.html", { "currentNews": currentNews[0],
                                                      "gallery": gallery })
        else:
            month = int(request.COOKIES.get("calendarMonth"))
            #возвращаем все новости, так как параметр slug пуст
            #формируем ответ с использованием шаблона новостей
            if month >= 1 and month <= 12:
                date = datetime.now().replace(month=month)
            else:
                date = datetime.now()
            response = render(request, "news.html", { "news": News.objects.filter(category=News.NEWS),
                                                        "monthEvents": preparedMonthEvents,
                                                        "currentMonth": date.strftime("%B") })
            #проверяем какой месяц календаря выбран
            #если ничего не было выбрано, то задаём текущий месяц в качестве значения
            if month < 1 or month > 12:
                response.set_cookie("calendarMonth", str(datetime.now().month))            
            return response
    else:
        #print("HELLO!")
        if date != None:
            try:
                parsedDate = datetime.strptime(date, "%Y-%m-%d")
            except ValueError:
                raise Http404

            dateEvents = Event.objects.filter(date=parsedDate)
            #возвращаем событие с конкретной датой и ссылкой
            if slug != None:
                currentEvent = Event.objects.get(date=parsedDate, slug=slug)
                return render(request, "events.html", { "currentEvent": currentEvent })
            else:
                return render(request, "events.html", { "news": News.objects.filter(category=News.NEWS), "dateEvents": dateEvents })
        else:
            return render(request, "news.html", { "news": News.objects.filter(category=News.NEWS),
                                                        "monthEvents": preparedMonthEvents,
                                                        "currentMonth": datetime.now().strftime("%b") })

#получаем события месяца как JSON объект (для использования в календаре)
def get_month_events_as_json(request, month=None):
    #пытаемся поставить русскую локаль (Windows-style)
    #в Unix/Linux системах сокращённая запись дня недели возвращается с точкой на конце
    #чтобы избежать этого, используем срез, подставляя cutValue
    try:
        locale.setlocale(locale.LC_ALL, 'ru')
        cutValue = None
    #Unix-style
    except:
        locale.setlocale(locale.LC_ALL, 'ru_RU')
        cutValue = -1

    #если месяц не указан, то получаем события текущего месяца    
    month=int(month)
    if month >= 1 and month <= 12:
        date = datetime.now().replace(month=month)
    else:
        date = datetime.now()
    monthEvents = Event.objects.filter(date__month=date.month)
    
    #инициализируем словарь для дальнейшего преобразования в JSON объект
    preparedMonthEvents = dict()
    preparedMonthEvents["month"] = date.strftime("%B")
    for i in range(0, len(monthEvents)):
        if len(monthEvents[i].title) > 20:
            preparedTitle = monthEvents[i].title[0:20] + "..."
        else:
            preparedTitle = monthEvents[i].title
        preparedMonthEvents[i] = ({ "numberOfDay": monthEvents[i].date.day,
                            "wordOfDay": monthEvents[i].date.strftime("%a")[0:cutValue],
                            "title": preparedTitle,
                            "slug": monthEvents[i].slug,
                            "date": monthEvents[i].date })
    return JsonResponse(preparedMonthEvents)

#представления для получения информации для школьников и студентов
def get_info_page(request, category=News.FOR_SCHOOLBOYS):
    try:
        info = News.objects.filter(category=category).earliest("publicationDate")
    except:
        info = []
    template = "for_schoolboys.html"
    if category == News.FOR_STUDENTS:
        template = "for_students.html"
    return render(request, template, { "info": info })