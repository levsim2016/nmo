from django.db import models
from ckeditor.fields import RichTextField
from django.conf import settings
from django.core.files.storage import FileSystemStorage
#from django.contrib.sites.models import Site

from unidecode import unidecode
from django.utils.text import slugify

from django.shortcuts import render, HttpResponse
from django.http import Http404
import json, os
from pathlib import Path

from .gallery import Gallery

class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name

class News(models.Model):
    previewImage = models.ImageField(verbose_name="Изображение для предварительного просмотра", storage=OverwriteStorage(), upload_to="images/news", unique=True)
    title = models.CharField(max_length=100, verbose_name = "Название новости")
    NEWS = "news"
    FOR_SCHOOLBOYS = "for-schoolboys"
    FOR_STUDENTS = "for-students"
    CATEGORIES = (
        (NEWS, "Обычные новости"),
        (FOR_SCHOOLBOYS, "Для школьников"),
        (FOR_STUDENTS, "Для студентов")
    )
    category = models.CharField(max_length=16, verbose_name="Категория новостей", choices=CATEGORIES, default=NEWS)
    publicationDate = models.DateField(verbose_name="Дата публикации", default="")
    content = RichTextField(verbose_name="Контент")
    gallery = models.OneToOneField(Gallery, related_name="gallery", on_delete=models.SET_NULL, verbose_name="Галерея изображений", null=True, blank=True)
    slug = models.SlugField(unique=True)
    link = models.CharField(max_length=256, verbose_name="Полная ссылка на новость", null=True)

    def save(self, *args, **kwargs):
        # previousState = News.objects.get(id=self.id)
        # if previousState.previewImage != None:
        #     previousState.previewImage.delete(False)
        self.slug = slugify(unidecode(self.title))
        routePath = os.path.join(self.category, self.slug)
        self.link = os.path.join(settings.CURRENT_HOST, routePath)
        super().save()

    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"