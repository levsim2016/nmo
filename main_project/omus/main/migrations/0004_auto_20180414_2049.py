# Generated by Django 2.0.3 on 2018-04-14 17:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20180414_0847'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='category',
            field=models.IntegerField(choices=[(0, 'Обычные новости'), (1, 'Для школьников'), (2, 'Для студентов')], default=0, verbose_name='Категория новостей'),
        ),
        migrations.AlterField(
            model_name='gallery',
            name='archive',
            field=models.FileField(blank=True, upload_to='', verbose_name='Архив изображений (в формате ZIP)'),
        ),
    ]
