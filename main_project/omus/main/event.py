from django.db import models
from ckeditor.fields import RichTextField
from django.conf import settings

from unidecode import unidecode
from django.utils.text import slugify
from django.urls import reverse
#from django.contrib.sites.models import Site

from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.http import JsonResponse, Http404
from datetime import datetime

import os

class Event(models.Model):
    title = models.CharField(max_length=100, verbose_name="Событие", default="")
    description = RichTextField(verbose_name="Описание")
    date = models.DateField(verbose_name="Дата события", default="")
    slug = models.SlugField(unique=True)
    link = models.CharField(max_length=256, verbose_name="Ссылка на событие", null=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(unidecode(self.title))
        routePath = os.path.join("events", self.slug)
        self.link = os.path.join(settings.CURRENT_HOST, routePath)
        super().save()

    # def as_view(request, date=None, slug=None):
    #     allNews = True
    #     if request.META["HTTP_HOST"] in request.META.get('HTTP_REFERER', ''):
    #         allNews = False

    #     news = []
    #     events = []
    #     #если мы обращаемся по адресу events/, то произойдёт редирект на страницу новостей
    #     if date is None and slug is None:
    #         return HttpResponseRedirect(reverse("news"))
    #     #если параметр, отвечающий за дату непустой, то ищем событие с этой датой
    #     elif date is not None:
    #         try:
    #             parsedDate = datetime.strptime(date, "%Y-%m-%d")
    #         except ValueError:
    #             raise Http404
    #         #если при этом не пуст параметр, отвечающий за ссылку на новость, то ищем сразу по этим 2 параметрам
    #         #получаем конкретное событие
    #         if slug is not None:
    #             events = Event.objects.filter(date=parsedDate, slug=slug)
    #             currentEvent = True
    #         #получаем события за некий день
    #         else:
    #             events = Event.objects.filter(date=parsedDate)
    #             dateEvent = True

    #     if len(events) == 0:
    #         raise Http404
        
    #     if allNews:
    #         for item in News.objects.all():
    #             news.append(item)
    #         return render(request, 'main/base.html', { 'news': news, 'events' : events })
    #     else:
    #         return JsonResponse(events)


    # def serialize_as_json(self):
    #     items = list(self.__dict__.items())[2:]
    #     items = dict(items)
    #     for key in items.keys():
    #         items[key] = str(items[key])
    #     return json.dumps(items)

    class Meta:
        verbose_name = "Событие"
        verbose_name_plural = "События"

    #def as_view(request):
