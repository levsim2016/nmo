"""omus URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.conf import settings
from django.conf.urls.static import static

from main import views

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('', views.redirect_to_news_page, name='news'),
    # path('news/', views.News.as_view, name='news'),
    # path('news/<slug:slug>', views.News.as_view, name='news'),
    # path('events/', views.Event.as_view, name='events'),
    # path('events/<date>', views.Event.as_view, name='events'),
    # path('events/<date>/<slug:slug>', views.Event.as_view, name='events')
    path('', views.redirect_to_news_page, name='news'),
    path('news/', views.get_main_page, name='news'),
    path('news/<slug:slug>', views.get_main_page, name='news'),
    path('ajax/get_month_events/<month>/', views.get_month_events_as_json),
    path('events/', views.redirect_to_news_page),
    path('events/<date>', views.get_main_page, { 'isNews' : False }),
    path('events/<date>/<slug:slug>', views.get_main_page, { 'isNews' : False }),
    path('for-schoolboys/', views.get_info_page),
    path('for-students/', views.get_info_page, { 'category' : views.News.FOR_STUDENTS })    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
